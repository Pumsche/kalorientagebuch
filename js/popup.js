/*
 letzte Änderung: 5.12.2007

 Beschreibung:
 -------------
 Ein Skript um ein Fenster in der Größe eines Bildes Öffnen.

 Einbinden:
 ----------
 <script src="popup.js"></script>
 
 <a href="grosses_bild.jpg" target="bild" class="popup"><img src="kleines_bild.jpg"></a>
*/
///////////////////////////////////////////////////////////
// Globale Definitionen
var popup_bgColor = '#fff';
var rahmen        = '1px solid black';
var abstand_w      = 1;
var abstand_h      = 1;
var center_popup  = true;
var popup_close   = 'click'; // Mögliche Werte: 'blur', 'click', ''

addEvent(window, 'load', function() {
	var all = document.links;

	for(var i = 0; i < all.length; i++) {
		if(all[i].className && all[i].className.indexOf('popup') > -1 )  {
			
			all[i].onclick = function () { showBild(this); return false;};
		}
	}
});

///////////////////////////////////////////////////////////
// showBild(a, name) - die Hauptfunktion.
var default_site = window.opera ? '' : '';
function showBild(a) {
	if(!a.target) a.target = "FotoPopup";
    var default_width   = 400;
    var default_height  = 200;
	if(a.target == window.name) a.target += '0';

    if(popup_close == 'blur' || !showFenster || showFenster.closed)
    showFenster = popUp(default_site, a.target, default_width, default_height);

    showFenster.document.open();
    showFenster.document.write( getHTML(a.href, a.title || a.alt || 'PicPopup') );
    showFenster.document.close();

	var img = document.createElement('img');
    img.ready = false;
    img.onload = function() { fitWin(this, showFenster); };
    img.src = a.href;
    if(img.complete) fitWin(img, showFenster);
    return false;
}
///////////////////////////////////////////////////////////
// fitWin(Image, window) - wird aus dem Popup aufgerufen.
function fitWin(i, win) {
	win.focus();
	if(i.ready) return;
	i.ready = true;
	var w = i.width;
	var h = i.height;
	var w_s = getWinSize(win);
	var r = 2 * parseInt(rahmen);
	
	win.resizeBy((w - w_s.width + ( 2 * abstand_w) + r), (h - w_s.height + ( 2 * abstand_h) + r) );
	if(center_popup) {
	w_s = getWinSize(win);
		win.moveTo( (screen.width - w_s.width) / 2, (screen.height - w_s.height) / 2 );
	}
}

/////////////////////////////////////////////////////////////////////
// getHTML(bild, titel, farbe)
function getHTML(src, title, bgcolor) {
	if(!title) title = 'kein Titel';
	if(!bgcolor) bgcolor = popup_bgColor;
	var NL = "\n";
	var text = '<!DOCTYPE HTML PUBLIC "-\/\/W3C\/\/DTD HTML 4.01\/\/EN" http:\/\/www.w3.org\/TR\/html4\/strict.dtd">\n'
	+ '<html>\n<head>' + NL
	+ '<title>' + title + '<\/title>' + NL
	+ '<style type="text/css">' + NL
	+ 'body{margin:0;padding:0;overflow:hidden;' + NL
	+ 'background-color:' + popup_bgColor + ';' + NL
	+ (popup_close.toLowerCase() == 'click' ? ' cursor:pointer;\n' : '')
	+ '}' + NL
	+ 'img{padding:0;'
	+ (rahmen ? 'border:' + rahmen + ';'  : '')
	+ 'margin-top:' + abstand_h +'px;' + NL
	+ 'margin-bottom:' + abstand_h +'px;' + NL
	+ 'margin-left:' + abstand_w +'px;' + NL
	+ 'margin-right:' + abstand_w +'px;' + NL
	+ '}\n' + NL
	+ '<\/style>' + NL
   + '<\/head>' + NL
   + '<body'
   + (popup_close.toLowerCase() == 'blur' ? ' onblur="self.close();"' : '')
   + '>'
   + '<img src="' + src + '" alt="' + title + '"'
   + (popup_close.toLowerCase() == 'click' ? ' onclick="window.close();"' : '')
   + '>' + NL
   + '<\/body><\/html>'
   ;
   return text;
}
/////////////////////////////////////////////////////////////////////
// Ein popup öffen
function popUp(url, fname, w, h) {
	var tmp = new Array();
	tmp[tmp.length] = 'resizable=yes';
	tmp[tmp.length] = 'scrollbars=no';
	if(w) tmp[tmp.length] = 'width=' + w;
	if(h) tmp[tmp.length] = 'height=' + h;

	return window.open(url, fname, tmp.join(','));
}
////////////////////////////////////////////////////////////
// getWinSize(window)
function getWinSize(win) {
	if(!win) win = window;
	// IE: Quirks- oder Standardmode
	var body = (win.document.compatMode && win.document.compatMode == "CSS1Compat") ? 
		win.document.documentElement : 
		win.document.body || null
	;
	return {
	width: (win.innerWidth || body.clientWidth),
	height: (win.innerHeight || body.clientHeight)
	};
}
var showFenster = null;
/////////////////////////////////////////////////////////////////////
// ... und am schluss alle Fenster schliessen.
window.onunload = function () {
	if(showFenster && !showFenster.closed) showFenster.close();
}